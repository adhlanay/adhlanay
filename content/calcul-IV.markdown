---
layout: page
title: Soft Matematic  - Tema 4
comments: true
sharing: false
category: Soft Matematic
tags: Teme, Anul-III
footer: true
---

A patra [ temă ]({filename}/pdf/IntroSoftMat-Laborator-04.pdf) de Laborator.
**Bonus:** Scrieți un program (nu neapărat în Matlab) care dă rezultatele corecte pentru problema 3,
punctele e) și f).

